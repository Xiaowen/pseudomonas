#!/usr/bin/env python
#-- Copyright (c) 2015 Xiaowen Lu
#-- Bioinformatics Group @ Wageningen University  --
#--
#--Script that extract CDS from gbk file


#Libraries to import
import sys
import os
import string
from Bio import SeqIO, SeqFeature

try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
import warnings
with warnings.catch_warnings(): # Don't display the SearchIO experimental warning.
    warnings.simplefilter("ignore")
    from Bio import SearchIO


def get_CDS_fasta(gbk_file_dir, faa_filename):


    output_handel =  open(faa_filename, "w")
    gbk_filelist = [f for f in os.listdir(gbk_file_dir) if os.path.isfile('/'.join((gbk_file_dir, f))) and not f.startswith('.')]

    for gbk in gbk_filelist:
        gbk_filename = os.path.join(gbk_file_dir, gbk)
        record = SeqIO.parse(gbk_filename, "genbank")

        for seq_record in record:
            print "Dealing with gbk record %s" % seq_record.id
            for seq_feature in seq_record.features:
                #screen the feather for each record, and get the CDS
                if seq_feature.type == "CDS":
                    assert len(seq_feature.qualifiers['translation']) == 1
                    seq_qualifiers = seq_feature.qualifiers.keys()
                    if "protein_id" in seq_qualifiers:
                        anno_1 = seq_feature.qualifiers['protein_id'][0]
                    else:
                        anno_1 = seq_feature.qualifiers['locus_tag'][0]

                    if "gene" in seq_qualifiers:
                        anno_2 = seq_feature.qualifiers['gene'][0]
                    else:
                        anno_2 = "?"

                    output_handel.write(">%s|%s|%s|%s\n%s\n" % (
                            gbk.split(".")[0],
                            seq_record.name,
                            anno_1,
                            anno_2,
                            seq_feature.qualifiers['translation'][0]))

    output_handel.close()




# gbk_file_dir = "/Users/Xiaowen/Documents/Pseudomonas_genomes_BGCs/gene_clusters/gene_clusters_II"
# faa_filename = "/Users/Xiaowen/Documents/Pseudomonas_genomes_BGCs/Pseudomonas_proteins_part2.fasta"
# get_CDS_fasta(gbk_file_dir, faa_filename)


if __name__ == "__main__":
    "Run this file from here as a script"
    #Check if parameters are provided; if not, exit with explanation
    if len(sys.argv) <= 2:
        print """Please provide as parameters a HMM file, a FASTA file and an output file"""
        sys.exit()

    #Read command-line parameters
    gbk_file_dir = sys.argv[1]
    faa_filename = sys.argv[2]

    #Run get_CDS_fasta
    get_CDS_fasta(gbk_file_dir, faa_filename)