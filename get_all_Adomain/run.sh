cd /Users/Xiaowen/Desktop/get_all_Adomain
#-- extract all CDS from the genbank file (.gbk) of all the identified gene clusters
python gbk2faa.py ./data Pseudomonas_proteins.fasta


#-- run the extract_A_domains.py
python extract_A_domains.py AMP_binding_3b2.hmm Pseudomonas_proteins.fasta Pseudomonas_A_domain.fasta A
