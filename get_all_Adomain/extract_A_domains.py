#!/usr/bin/env python
## Copyright (c) 2014 Marnix H. Medema
## Max Planck Institute for Marine Microbiology
## Microbial Genomics & Bioinformatics Research Group

###Script that extract A domains out of protein sequences

#Libraries to import
import sys
import os
import string
import subprocess
#sys.path.append('/Users/Xiaowen/Documents/Pseudomonas_genomes_BGCs/domain_extraction_script/Bio')


try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO
import warnings
with warnings.catch_warnings(): # Don't display the SearchIO experimental warning.
    warnings.simplefilter("ignore")
    from Bio import SearchIO


#Global settings
global cpu
##Number of CPU cores used for multiprocessing
cpu = 4

def execute(commands, input=None):
    "Execute commands in a system-independent manner"

    if input is not None:
        stdin_redir = subprocess.PIPE
    else:
        stdin_redir = None

    proc = subprocess.Popen(commands, stdin=stdin_redir,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    try:
        out, err = proc.communicate(input=input)
        retcode = proc.returncode
        return out, err, retcode
    except OSError, e:
        print "%r %r returned %r" % (commands, input[:40], e)
        raise

def run_hmmsearch(query_hmmfile, fasta_file):
    "Run hmmsearch"
    command = ["hmmsearch_3", "--cpu", "4","--cut_tc", "--domtblout", "temp.out", query_hmmfile, fasta_file]
    try:
        out, err, retcode = execute(command)
	out = open("temp.out","r").read()
    except OSError:
        return []
    res_stream = StringIO(out)
    results = list(SearchIO.parse(res_stream, 'hmmsearch3-domtab'))
    return results

def read_fasta_file(fastafile):
    "Get sequences from fasta file and store in dictionary"
    ###My idea:
    infile = open(fastafile).read()
    entries = infile.split(">")[1:]
    #print entries
    fastadict = {} #Accession as key, sequence as value
    for entry in entries:
        accession = entry.partition("\n")[0].replace("\r","")
        #print accession
        sequence = entry.partition("\n")[2].partition("\n")[0]
        #print sequence
        fastadict[accession] = sequence  
    ###

    ###TO BE DONE
    # - Open FASTA file
    # - Separate by entries
    # - For each entry, store information to fastadict
    
    return fastadict

def run_HMMer(hmmfile, fastafile):
    "Run HMMer to find start and end sites of A domains"
    runresults = run_hmmsearch(hmmfile, fastafile)
    results_by_id = {}
    for runresult in runresults:
        #Store results in dictionary by NRPS accession
        for hsp in runresult.hsps:
            if not results_by_id.has_key(hsp.hit_id):
                results_by_id[hsp.hit_id] = [hsp]
            else:
                results_by_id[hsp.hit_id].append(hsp)
    return results_by_id

def write_fasta(fastadict, outfile, hmmer_results, domain_ab):
    #For each HMM, print a FASTA file with all domain hits
    out_file = open(outfile, "w")
    domnr = 1
    for cds in hmmer_results.keys():
        #Get sequence from fastadicts
        ###TO BE DONE
        cds_sequence = fastadict[cds]
        ###TO BE DONE
        
        #For each hit, write out domain name + sequence in FASTA format
        for hit in hmmer_results[cds]:
            domain_sequence = cds_sequence[hit.hit_start:hit.hit_end]
            domain_name = "%s_%s%s" % (cds, domain_ab, domnr)
            out_file.write(">%s\n%s\n" % (domain_name, domain_sequence))
            domnr += 1
        domnr = 1
    out_file.close()


if __name__ == "__main__":
    "Run this file from here as a script"
    #Check if parameters are provided; if not, exit with explanation
    if len(sys.argv) <= 4:
        print """Please provide as parameters a HMM file, a FASTA file and an output file"""
        sys.exit()

    #Read command-line parameters
    hmmfile = sys.argv[1]
    fastafile = sys.argv[2]
    outfile = sys.argv[3]
    domain_ab = sys.argv[4]
    
    #Read FASTA file
    fastadict = read_fasta_file(fastafile)

    #Run HMMer
    hmmer_results = run_HMMer(hmmfile, fastafile)

    #Write FASTA file with domains
    write_fasta(fastadict, outfile, hmmer_results, domain_ab)


